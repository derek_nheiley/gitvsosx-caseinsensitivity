![SCREENSHOTS](https://raw.github.com/derekneil/gitvsosx-caseinsensitivity/master/osxpartition.png "OSX Partitions")

OSX isn't case sensitive, and this can cause problems with git, even if you have core.ignorecase=false set in git.

Each branch will test a different scenario for filename case sensitivity, and some branchs will mix commits on both types of file systems to illustrate the problem.
